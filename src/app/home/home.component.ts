import { AfterContentChecked, AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { combineLatest, Subscription, timer, } from 'rxjs';
import { map, share } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  Time = new Date();
  subscription!: Subscription;
  subscription2!: Subscription;
  color: any
  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.subscription = timer(0, 1000)
      .pipe(
        map(() => new Date()),
      )
      .subscribe(time => {
        this.Time = time;
      }).add(timer(0, 4000).subscribe(() => {
        this.color = '#' + ('000000' + Math.floor(0x1000000 * Math.random()).toString(16)).slice(-6)
      }))
  }


  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }



}
