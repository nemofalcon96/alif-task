import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hall',
  templateUrl: './hall.component.html',
  styleUrls: ['./hall.component.scss']
})
export class HallComponent implements OnInit {
  @Input() color:any;
  constructor() { }
  ngOnInit(): void {
  }
}
